subject = null
AI = require('bot/ai')

describe 'bot/AI', ->
  instance = null

  beforeEach ->
    instance = new AI()

  describe 'response', ->
    beforeEach ->
      subject = -> instance.response()

    it 'should ping if no response is yet recorded', ->
      response = subject()
      expect(response.msgType).toEqual 'ping'
      expect(response.data).toEqual {}

    context 'when some responses have been queued', ->
      nextResponse = null

      beforeEach ->
        instance.queueResponse nextResponse = { msgType: 'hello' }
        instance.queueResponse { msgType: 'world' }

      it 'should return the next response in the queue', ->
        response = subject()
        expect(response).toBe nextResponse

  context 'during a race', ->
    beforeEach ->
      yourCarMessage = {"msgType":"yourCar","data":{"name":"Banzai Kings","color":"red"},"gameId":"47845164-d50d-4a24-acab-1d28be9ef6de"}
      instance.yourCar(yourCarMessage)

      gameInitMessage = {"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100},{"length":100},{"length":100},{"length":100,"switch":true},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":200,"angle":22.5,"switch":true},{"length":100},{"length":100},{"radius":200,"angle":-22.5},{"length":100},{"length":100,"switch":true},{"radius":100,"angle":-45},{"radius":100,"angle":-45},{"radius":100,"angle":-45},{"radius":100,"angle":-45},{"length":100,"switch":true},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100,"switch":true},{"radius":100,"angle":45},{"radius":100,"angle":45},{"length":62},{"radius":100,"angle":-45,"switch":true},{"radius":100,"angle":-45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"length":100,"switch":true},{"length":100},{"length":100},{"length":100},{"length":90}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300,"y":-44},"angle":90}},"cars":[{"id":{"name":"Banzai Kings","color":"red"},"dimensions":{"length":40,"width":20,"guideFlagPosition":10}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"47845164-d50d-4a24-acab-1d28be9ef6de"}
      instance.gameInit(gameInitMessage)

    describe 'carPositions', ->

    describe 'myCar', ->
      beforeEach ->
        subject = -> instance.myCar()

      context 'when "carPositions" has been received', ->
        myPosition = null

        beforeEach ->
          carPositionsMessage = { "msgType": "carPositions", "data": [
              {
                "id": { "name": "Some other car", "color": "blue" },
                "angle": 0,
                "piecePosition": {
                  "pieceIndex": 0, "inPieceDistance": 1, "lane": { "startLaneIndex": 0, "endLaneIndex": 0 }, "lap": 0
                }
              },
              myPosition = {
                "id": { "name": "Banzai Kings", "color": "red" },
                "angle": 0,
                "piecePosition": {
                  "pieceIndex": 0, "inPieceDistance": 2, "lane": { "startLaneIndex": 0, "endLaneIndex": 0 }, "lap": 0
                }
              },
              {
                "id": { "name": "A third car", "color": "green" },
                "angle": 0,
                "piecePosition": {
                  "pieceIndex": 0, "inPieceDistance": 4, "lane": { "startLaneIndex": 0, "endLaneIndex": 0 }, "lap": 0
                }
              }
            ], "gameId": "47845164-d50d-4a24-acab-1d28be9ef6de", "gameTick": 1
          }
          console.log("carPositionsMessage", JSON.stringify carPositionsMessage)
          instance.carPositions( carPositionsMessage )

        it 'should return the position of my car', ->
          expect(subject()).toBe(myPosition)

    describe 'lengthOfPiece', ->
      piece = null
      laneIndex = null

      beforeEach ->
        subject = -> instance.lengthOfPiece(piece(), laneIndex())

      context 'for a straight piece', ->
        length = null

        beforeEach ->
          piece = -> {"length": length = 100,"switch":true}
          laneIndex = -> 0

        it 'should return the length of the piece', ->
          expect(subject()).toEqual(length)

      context 'for a curved piece', ->
        expectedLength = null

        beforeEach ->
          angle = 45
          radius = 100
          distanceFromCenter = -10

          piece = -> { radius: radius, angle: angle }
          laneIndex = -> 0

          expectedLength = (angle / 360.0) * 2 * Math.PI * (radius - distanceFromCenter)

        it 'should return the length of the piece', ->
          expect(subject()).toEqual(expectedLength)