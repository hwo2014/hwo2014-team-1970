Track = require('bot/track')

subject = null

describe 'Track', ->
  instance = null

  beforeEach ->
    instance = new Track(trackData)

  describe 'laneRadius', ->
    piece = null
    laneIndex = null

    beforeEach ->
      subject = -> instance.laneRadius(piece(), laneIndex())

    context 'on a piece curved to the right', ->
      beforeEach -> piece = -> { angle: 45, radius: 100 }

      context 'for lane 0', ->
        beforeEach -> laneIndex = -> 0
        it 'should be +10', -> expect(subject()).toEqual(100 + 10)

      context 'for lane 1', ->
        beforeEach -> laneIndex = -> 1
        it 'should be -10', -> expect(subject()).toEqual(100 + -10)

    context 'on a piece curved to the left', ->
      beforeEach -> piece = -> { angle: -45, radius: 100 }

      context 'for lane 0', ->
        beforeEach -> laneIndex = -> 0
        it 'should be -10', -> expect(subject()).toEqual(100 + -10)

      context 'for lane 1', ->
        beforeEach -> laneIndex = -> 1
        it 'should be +10', -> expect(subject()).toEqual(100 + 10)

  describe 'laneLengthOnPiece', ->
    piece = null
    lane = null

    beforeEach ->
      subject = -> instance.laneLengthOnPiece(piece(), lane())

    context 'for a straight piece', ->
      beforeEach -> piece = -> { length: 100 }

      context 'for lane 0', ->
        beforeEach -> lane = -> 0

        it 'should be the length of the piece', ->
          expect(subject()).toEqual(piece().length)

      context 'for lane 1', ->
        beforeEach -> lane = -> 1

        it 'should be the length of the piece', ->
          expect(subject()).toEqual(piece().length)

    context 'for a piece curved to the right', ->
      expectedLength = null

      beforeEach -> piece = -> { angle: 45, radius: 100 }

      context 'for lane 0', ->
        beforeEach ->
          lane = -> 0
          expectedLength = -> (45/360.0) * 2 * Math.PI * instance.laneRadius(piece(), lane())

        it 'should be the length of the piece', ->
          expect(subject()).toEqual(expectedLength())

      context 'for lane 1', ->
        beforeEach ->
          lane = -> 1
          expectedLength = -> (45/360.0) * 2 * Math.PI * instance.laneRadius(piece(), lane())

        it 'should be the length of the piece', ->
          expect(subject()).toEqual(expectedLength())

    context 'for a piece curved to the left', ->
      expectedLength = null

      beforeEach -> piece = -> { angle: -45, radius: 100 }

      context 'for lane 0', ->
        beforeEach ->
          lane = -> 0
          expectedLength = -> (45/360.0) * 2 * Math.PI * instance.laneRadius(piece(), lane())

        it 'should be the length of the piece', ->
          expect(subject()).toEqual(expectedLength())

      context 'for lane 1', ->
        beforeEach ->
          lane = -> 1
          expectedLength = -> (45/360.0) * 2 * Math.PI * instance.laneRadius(piece(), lane())

        it 'should be the length of the piece', ->
          expect(subject()).toEqual(expectedLength())


trackData = {
  "id":            "keimola",
  "name":          "Keimola",
  "pieces":        [
    {
      "length": 100
    },
    {
      "length": 100
    },
    {
      "length": 100
    },
    {
      "length": 100,
      "switch": true
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 200,
      "angle":  22.5,
      "switch": true
    },
    {
      "length": 100
    },
    {
      "length": 100
    },
    {
      "radius": 200,
      "angle":  -22.5
    },
    {
      "length": 100
    },
    {
      "length": 100,
      "switch": true
    },
    {
      "radius": 100,
      "angle":  -45
    },
    {
      "radius": 100,
      "angle":  -45
    },
    {
      "radius": 100,
      "angle":  -45
    },
    {
      "radius": 100,
      "angle":  -45
    },
    {
      "length": 100,
      "switch": true
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 200,
      "angle":  22.5
    },
    {
      "radius": 200,
      "angle":  -22.5
    },
    {
      "length": 100,
      "switch": true
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "length": 62
    },
    {
      "radius": 100,
      "angle":  -45,
      "switch": true
    },
    {
      "radius": 100,
      "angle":  -45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "radius": 100,
      "angle":  45
    },
    {
      "length": 100,
      "switch": true
    },
    {
      "length": 100
    },
    {
      "length": 100
    },
    {
      "length": 100
    },
    {
      "length": 90
    }
  ],
  "lanes":         [
    {
      "distanceFromCenter": -10,
      "index":              0
    },
    {
      "distanceFromCenter": 10,
      "index":              1
    }
  ],
  "startingPoint": {
    "position": {
      "x": -300,
      "y": -44
    },
    "angle":    90
  }
}