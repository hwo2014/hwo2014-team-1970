net        = require("net")
JSONStream = require('JSONStream')
_ = require('underscore')
Q = require('q')

serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

console.log("I'm", botName, "connecting to", serverHost + ":" + serverPort)

TRACKS = ['keimola', 'germany', 'usa']

testOptions = (trackName, options) ->
  console.log('Beginning test run in', trackName)
  deferred = Q.defer()
  client = net.connect serverPort, serverHost, () ->
    send({ msgType: "joinRace", data: { botId: { name: botName, key: botKey }, trackName: trackName, carCount: 1 } })

  send = (json) ->
    client.write JSON.stringify(json)
    client.write '\n'

  jsonStream = client.pipe(JSONStream.parse())

  AI = require("./bot/ai")
  ai = new AI(options)

  log = options.log || console.log

  ended = false

  jsonStream.on 'data', (data) ->
    return if ended
    log("server: #{JSON.stringify data}")

    if data.msgType == 'gameEnd'
      console.log "  Succeeded on #{trackName}"
      deferred.resolve [trackName, data.data]

    try
      ai[data.msgType]( data ) if ai[data.msgType]
    catch error
      console.log "  Failed on #{trackName} with", error
      deferred.reject [trackName, error]
      client.end()
      ended = true
      return

    response = ai.response()
    log("bot: #{JSON.stringify response}")
    send(response)
    log ' '

  jsonStream.on 'error', (args...) ->
    log "error - disconnected"
    deferred.reject [trackName, args]

  deferred.promise

binarySearch = (testedValue, minimum, maximum, seeking, threshold, options) ->
  searchLog = []
  step(searchLog, options, testedValue, minimum, maximum, seeking, threshold)

steps = 0

# Perform the test with the next value and return a promise of its completion
step = (searchLog, options, testedValue, minimum, maximum, seeking, threshold) ->
  deferred = Q.defer()

  newValue = minimum + (maximum - minimum) / 2.0
  options = _(options).clone()
  options[testedValue] = newValue

  steps += 1
  console.log ' '
  console.log "Step ##{steps}..."
  console.log 'minimum =', minimum, ', maximum =', maximum, ', newValue =', newValue
  console.log 'options =', options

  testRuns = _(TRACKS).map (trackName) -> testOptions(trackName, options ).then( (result) -> [trackName, result] )

  Q.allSettled(testRuns).spread( (testRunPromises...) ->
    allSucceeded = true
    stepResult = {}
    for testRunPromise in testRunPromises
      # to do: consider game time not just success/failure
      succeeded = testRunPromise.state == 'fulfilled'
      allSucceeded = allSucceeded && succeeded
      testResult = testRunPromise.value || testRunPromise.reason
      stepResult[testResult[0]] = testResult[1]

    console.log stepResult
    searchLog.push [ options, stepResult ]

    if allSucceeded
      if seeking == 'highest'
        minimum = newValue
      else
        maximum = newValue
      console.log('Succeeded!')
    else
      if seeking == 'highest'
        maximum = newValue
      else
        minimum = newValue
      console.log('Failed :(')
  ).finally ->
    if maximum - minimum < threshold
      deferred.resolve( searchLog )
      return # Enough testing
    deferred.resolve( step(searchLog, options, testedValue, minimum, maximum, seeking, threshold) )

  deferred.promise


( ->
  # TODO: get this from command line
#  testedValue = 'maximumDegreesPerTickEnteringBend'
#  minimum = 5.965625
#  maximum = 10
#  seeking = 'highest'
#  threshold = 0.01

  testedValue = 'maximumDegreesPerTickEnteringBend'
  minimum = 6.1862548828125
  maximum = 15
  seeking = 'highest'
  threshold = 0.01

#  testedValue = 'decelerationConstant'
#  minimum = 0.015
#  maximum = 0.03905940
#  seeking = 'highest'
#  threshold = 0.0000001

  binarySearch(testedValue, minimum, maximum, seeking, threshold, {
    log: ->
    testedValue: testedValue
    decelerationConstant: 0.02
    maximumDegreesPerTickEnteringBend: 6.1862548828125
    angleImportance: 1.607142857
  })
)().then (result) ->
  console.log "Completed binary search"
  console.log JSON.stringify result
#  console.log ' '
#  lastSuccessfulRun = _(result.reverse()).find((res) -> res[1] != 'crash')
#  finalOptions = lastSuccessfulRun[0]
#  testedValue = finalOptions.testedValue
#  console.log 'Final', testedValue, '=', finalOptions[testedValue]

