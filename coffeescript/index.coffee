net        = require("net")
JSONStream = require('JSONStream')

serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort)

client = net.connect serverPort, serverHost, () ->
  send({ msgType: "join", data: { name: botName, key: botKey }})

send = (json) ->
  client.write JSON.stringify(json)
  client.write '\n'

jsonStream = client.pipe(JSONStream.parse())

AI = require("./bot/ai")
ai = new AI()

jsonStream.on 'data', (data) ->
  console.log("server: #{JSON.stringify data}")
  ai[data.msgType]( data ) if ai[data.msgType]
  response = ai.response()
  console.log("bot: #{JSON.stringify response}")
  send(response)

jsonStream.on 'error', ->
  console.log "error - disconnected"
