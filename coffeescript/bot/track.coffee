_ = require('underscore')

class Track
  constructor: (@data) ->
    piece.index = i for piece, i in @data.pieces

  laneLengthOnPiece: (pieceIndex, laneIndex) ->
    if @piece(pieceIndex).angle?
      (Math.abs(@piece(pieceIndex).angle) / 360.0) * 2 * Math.PI * @laneRadius(@piece(pieceIndex), laneIndex)
    else
      @piece(pieceIndex).length

  laneRadius: (piece, laneIndex) ->
    if piece.angle > 0 # Right turn
      piece.radius + @lane( laneIndex ).distanceFromCenter * -1
    else # Left turn
      piece.radius + @lane( laneIndex ).distanceFromCenter

  lane: (index) -> @data.lanes[index]
  piece: (index) -> @data.pieces[index]

  availableLanesForSwitch: (currentLaneIndex) ->
    result = {}
    if (leftIndex = currentLaneIndex - 1) >= 0
      result.left = leftIndex
    if (rightIndex = currentLaneIndex + 1) < @data.lanes.length
      result.right = rightIndex
    result

  distanceUntilNextSwitch: (pieceIndex, originLaneIndex, destLaneIndex) ->
    nextSwitchIndex = @nextSwitchPiece( pieceIndex ).index
#    console.log('next switch occurs at piece', nextSwitchIndex)

    distanceOverSwitchOnOriginLane = @laneLengthOnPiece(pieceIndex, originLaneIndex)
    distanceOverSwitchOnDestLane = @laneLengthOnPiece(pieceIndex, destLaneIndex)
    distanceOverSwitch = (distanceOverSwitchOnOriginLane + distanceOverSwitchOnDestLane) / 2.0

    # TODO: check that -1/+1 doesn't give a piece index out of array bounds
    distanceOverOtherPieces = @distanceBetweenPieces(pieceIndex + 1, nextSwitchIndex - 1, destLaneIndex)
#    console.log 'distance on lane', laneIndex, '=', result
    distanceOverOtherPieces + distanceOverSwitch

  nextSwitchPiece: (pieceIndex) ->
    if pieceIndex > 0
      pieces = @piecesBetween(pieceIndex + 1, pieceIndex - 1)
    else
      pieces = @data.pieces[1..]

    _(pieces).find (piece) -> piece.switch

  nextBendPieceIndex: (pieceIndex) ->
    if pieceIndex > 0
      pieces = @piecesBetween(pieceIndex + 1, pieceIndex - 1)
    else
      pieces = @data.pieces[1..]

    (_(pieces).find (piece) -> piece.angle?).index

  piecesBetween: (pieceIndex1, pieceIndex2) ->
    if pieceIndex1 > pieceIndex2
      @data.pieces[pieceIndex1..].concat @data.pieces[..pieceIndex2]
    else
      @data.pieces[pieceIndex1..pieceIndex2]

  distanceBetweenPieces: (pieceIndex1, pieceIndex2, laneIndex) ->
    pieces = @piecesBetween(pieceIndex1, pieceIndex2)

    _(pieces).reduce ((memo, piece) =>
      length = @laneLengthOnPiece(piece.index, laneIndex)
#      console.log 'length of piece', piece.index, length
      memo + length
    ), 0


module.exports = Track