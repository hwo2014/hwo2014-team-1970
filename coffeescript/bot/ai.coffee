_ = require('underscore')
Track = require('./track')

class AI
  constructor: (options = {}) ->
    @responses = []

    # Maximum speed we can handle going around bends, in degrees per second
    # Calculated from speed as:
    #  speed = track units / second
    #  deg/sec = (tu/s) * (angle of piece/length of piece)
    @maximumDegreesPerTickEnteringBend = options.maximumDegreesPerTickEnteringBend || 4.187889099121094
    @maximumDegreesPerTickExitingBend = options.maximumDegreesPerTickExitingBend || 6.264755249023438
    @angleImportance = options.angleImportance || 1.607142857
    @log = options.log || console.log

    # The amount of deceleration we experience with zero throttle (as a multiplier of current speed)
    @decelerationConstant = options.decelerationConstant || 0.02

    @logVariables()
    @maximumSpeed = 100

  #---------------------------------------------------------------------------------------------------------------------
  # Messaging
  #---------------------------------------------------------------------------------------------------------------------

  gameInit: (message) ->
    @gameData = message.data
    @track = new Track(@gameData.race.track)

  # Sent at beginning of game
  yourCar: (message) -> @myCarId = message.data

  # I'm not sure the server will accept messages in response to `gameStart`, so we have a similar check in
  # `carPositions`.
  gameStart: (message) ->
    @setThrottle 1.0 # Full steam ahead!

  tournamentEnd: -> @logVariables()

  # Sent once per tick with the position of everything on the field
  carPositions: (message) ->
    return unless message.gameTick?

    @previousCarPositions = @latestCarPositions
    @latestCarPositions = message.data

    currentSpeed = @currentSpeed()
    desiredSpeed = @desiredSpeed()
    @log("currentSpeed() = #{currentSpeed}")
    @log("desiredSpeed() = #{desiredSpeed}")

    # The first thing we want to do is take off
    if !@sentInitialThrottleMessage
      @setThrottle(1)
      @sentInitialThrottleMessage = true
      return

    # Take the earliest opportunity to send a switch command
    if @needToSendSwitchDecision()
      if @performSwitch()
        @log '- Switching'
        return

    # Otherwise set the throttle
    if currentSpeed < desiredSpeed
      if @needToBrake()
        @setThrottle(0)
        @log('- Braking for upcoming bend')
      else
        @setThrottle(1)
        @log('- Accelerating')
    else
      @setThrottle(0)
      @log('- Going too fast, braking')

  crash: ->
    @logVariables()
    throw 'crash'

  response: -> @responses.shift() || @ping()
  queueResponse: (response) -> @responses.push(response)
  flushResponses: -> @responses.splice(0, @responses.length)

  ping: -> { msgType: "ping", data: {} }

  setThrottle: (@throttle) ->
    if @throttle < 0
      @throttle = 0
    if @throttle > 1
      @throttle = 1
    @log("setting throttle to", @throttle)
    @queueResponse({ msgType: 'throttle', data: @throttle })

  switchLane: (lane) ->
    throw "Invalid lane identifier: '#{lane}'" unless lane == 'Left' || lane == 'Right'
    @log("\nSwitching to #{lane} lane\n")
    @queueResponse({ msgType: 'switchLane', data: lane })

  #---------------------------------------------------------------------------------------------------------------------
  # Utility functions
  #---------------------------------------------------------------------------------------------------------------------

  logVariables: ->
    @log '@maximumDegreesPerTickEnteringBend =', @maximumDegreesPerTickEnteringBend
    @log '@maximumDegreesPerTickExitingBend =', @maximumDegreesPerTickExitingBend
    @log '@decelerationConstant =', @decelerationConstant

  needToBrake: ->
    currentPieceIndex = @myCar().piecePosition.pieceIndex
    nextBendPieceIndex = @track.nextBendPieceIndex(currentPieceIndex)

    currentLaneIndex = @myCar().piecePosition.lane.endLaneIndex
    if @lastSwitchIndex < nextBendPieceIndex
      expectedLaneIndexOnBend = @switchingToLane || currentLaneIndex
    else
      expectedLaneIndexOnBend = currentLaneIndex

    inPieceDistance = @myCar().piecePosition.inPieceDistance

    # NOTE: the bend we're considering might be after the switch, but there will be no other bend pieces before it
    # TODO: calculate distance across switch properly
    # TODO: check that this -1 doesn't give us a negative array index
    distanceToBend = @track.distanceBetweenPieces(currentPieceIndex, nextBendPieceIndex - 1, currentLaneIndex) - inPieceDistance
    speedRequiredAtBend = @desiredSpeedOnPiece(nextBendPieceIndex, expectedLaneIndexOnBend, 0, @myCar().angle)
    currentSpeed = @currentSpeed()
    amountWeNeedToDecelerate = currentSpeed - speedRequiredAtBend
    timeToReachBend = distanceToBend / currentSpeed # At current speed (obviously a problem)
    deceleration = @decelerationConstant * currentSpeed # % of current speed

    # How much will we decelerate if we start braking now?
    amountDeceleratedIfWeBrakeNow = timeToReachBend * deceleration

    @log('nextBendPieceIndex            =', nextBendPieceIndex)
    @log('distanceToBend                =', distanceToBend)
    @log('speedRequiredAtBend           =', speedRequiredAtBend)
    @log('currentSpeed                  =', currentSpeed)
    @log('amountWeNeedToDecelerate      =', amountWeNeedToDecelerate)
    @log('amountDeceleratedIfWeBrakeNow =', amountDeceleratedIfWeBrakeNow)
    amountWeNeedToDecelerate >= amountDeceleratedIfWeBrakeNow

  needToSendSwitchDecision: ->
    currentPieceIndex = @myCar().piecePosition.pieceIndex
    nextSwitchIndex = @track.nextSwitchPiece( currentPieceIndex ).index
    # If we haven't already sent an instruction for this switch piece
    nextSwitchIndex != @lastSwitchIndex

  # Switching strategy:
  #   At a given switch piece, we want to be on the lane with the shortest distance to the next switch
  performSwitch: ->
    currentPieceIndex = @myCar().piecePosition.pieceIndex
    nextSwitchIndex = @track.nextSwitchPiece( currentPieceIndex ).index
    # We've made a decision for this switch, don't reconsider it
    @lastSwitchIndex = nextSwitchIndex

    currentLaneIndex = @myCar().piecePosition.lane.endLaneIndex
    availableLanes = @track.availableLanesForSwitch(currentLaneIndex)
    @log(' ')
    @log('considering switch on piece', nextSwitchIndex)
    @log 'currentLaneIndex =', currentLaneIndex
    bestOption = {
      lane: 'Current',
      laneIndex: currentLaneIndex,
      distance: @track.distanceUntilNextSwitch(nextSwitchIndex, currentLaneIndex, currentLaneIndex)
    }
    @log("Distance to next switch on THIS lane:", bestOption.distance)

    if availableLanes.left?
      lengthAlongLeftLane = @track.distanceUntilNextSwitch(nextSwitchIndex, currentLaneIndex, availableLanes.left)
      @log("Distance to next switch on LEFT lane:", lengthAlongLeftLane)
      if lengthAlongLeftLane < bestOption.distance
        bestOption = {
          lane: 'Left',
          laneIndex: availableLanes.left,
          distance: lengthAlongLeftLane
        }
    if availableLanes.right?
      lengthAlongRightLane = @track.distanceUntilNextSwitch(nextSwitchIndex, currentLaneIndex, availableLanes.right)
      @log("Distance to next switch on RIGHT lane:", lengthAlongRightLane)
      if lengthAlongRightLane < bestOption.distance
        bestOption = {
          lane: 'Right',
          laneIndex: availableLanes.right,
          distance: lengthAlongRightLane
        }

    if bestOption.lane != 'Current'
      @switchLane(bestOption.lane)
      @switchingToLane = bestOption.laneIndex
      true # Perform the switch
    else
      @switchingToLane = null
      false

  lastPiece: ->
    return null unless @previousCarPositions
    lastPosition = @positionOfCar(@previousCarPositions, @myCarId).piecePosition
    @track.piece(lastPosition.pieceIndex)

  currentPiece: -> @track.piece(@myCar().piecePosition.pieceIndex)

  nextPiece: -> @track.piece((@myCar().piecePosition.pieceIndex + 1) % @gameData.race.track.pieces.length)

  desiredSpeed: ->
    piecePosition = @myCar().piecePosition
    currentPieceIndex = piecePosition.pieceIndex
    inPieceDistance = piecePosition.inPieceDistance
    currentLaneIndex = piecePosition.lane.endLaneIndex
    pieceAngle = @track.piece(currentPieceIndex).angle
    carAngle = @myCar().angle
    @log('carAngle =', carAngle)
    @log('pieceAngle =', pieceAngle)
    result = @desiredSpeedOnPiece(currentPieceIndex, currentLaneIndex, inPieceDistance, carAngle)
    @log('desiredSpeed =', result)
    result

  desiredSpeedOnPiece: (pieceIndex, laneIndex, inPieceDistance, carAngle) ->
    piece = @track.piece( pieceIndex )
    pieceAngle = piece.angle
    if pieceAngle
      # Maximum speed we can handle going around bends, in degrees per second
      # Calculated from speed as:
      #  speed = track units / second
      #  deg/sec = (tu/s) * (angle of piece/length of piece)

      # If I continue at my current speed around a bend with `angle` and `radius`, I will go how many deg/s?
      #  speed = track units / tick, e.g. 10tu/tick
      #  deg/track unit = angle/lengthOfPiece
      #  deg/tick = (track units/tick) / (deg/track unit) = (tu/tick) / (angle/lengthOfPiece)
      #  deg/tick = (tu/tick) / (angle/lengthOfPiece)
      # (tu/tick) = (deg/tick) * (angle/lengthOfPiece)
      absolutePieceAngle = Math.abs(pieceAngle)
      currentLaneIndex = @myCar().piecePosition.lane.endLaneIndex

      if piece.switch && @switchingToLane?
        destLaneIndex = @switchingToLane
        lengthOnCurrentLane = @track.laneLengthOnPiece(pieceIndex, currentLaneIndex)
        lengthOnDestLane = @track.laneLengthOnPiece(pieceIndex, destLaneIndex)
        lengthOfPiece = (lengthOnCurrentLane + lengthOnDestLane) / 2.0
      else
        lengthOfPiece = @track.laneLengthOnPiece(pieceIndex, currentLaneIndex)

      if inPieceDistance / lengthOfPiece < 0.5
        absoluteDesiredSpeed = @maximumDegreesPerTickEnteringBend * lengthOfPiece / absolutePieceAngle
      else
        absoluteDesiredSpeed = @maximumDegreesPerTickExitingBend * lengthOfPiece / absolutePieceAngle

      @log('absoluteDesiredSpeed =', absoluteDesiredSpeed)
      # If the car is facing out of the bend (e.g. if we just came off a bend in the opposite direction) we don't need
      # to worry about our slip angle
      if carAngle/carAngle == pieceAngle/pieceAngle
        # TODO: rethink this
        slipAngleMagnitude = Math.abs(carAngle) / 90 # % of 90 degrees
        result = (1.0 - (slipAngleMagnitude * @angleImportance)) * absoluteDesiredSpeed
#        if result < 0 then 0 else result
      else
        absoluteDesiredSpeed
    else
      # Can't possibly crash on the straight
      @maximumSpeed

  currentSpeed: -> @distanceMoved() # Distance units per tick

  distanceMoved: ->
    currentPosition = @myCar().piecePosition
    return currentPosition.inPieceDistance unless @previousCarPositions

    lastPosition = @positionOfCar(@previousCarPositions, @myCarId).piecePosition

    if lastPosition.pieceIndex == currentPosition.pieceIndex
      currentPosition.inPieceDistance - lastPosition.inPieceDistance
    else
      laneIndex = lastPosition.lane.startLaneIndex
      lengthOfLastPiece = @track.laneLengthOnPiece(lastPosition.pieceIndex, laneIndex)
      distanceMovedOnLastPiece = lengthOfLastPiece - lastPosition.inPieceDistance
      distanceMovedOnLastPiece + currentPosition.inPieceDistance

  myCar: -> @positionOfCar(@latestCarPositions, @myCarId)

  positionOfCar: (positions, id) ->
    _(positions).find (car) => car.id.name == id.name && car.id.color == id.color

module.exports = AI